# CS371g: Generic Programming Integer Repo

* Name: Shuyan Li, Yichen Liu

* EID: sl45644, yl33325

* GitLab ID: shuyan_99, tt0suzy

* HackerRank ID: shuyan_li, 98yichenliu

* Git SHA: cf47da34ffb199e47b084d0b962b6dd58fab37fb

* GitLab Pipelines: https://gitlab.com/shuyan_99/cs371g-integer/-/pipelines

* Estimated completion time: 10 hrs

* Actual completion time: 15 hrs

* Comments: N/A
