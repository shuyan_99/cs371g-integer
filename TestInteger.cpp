// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"


#include "Integer.hpp"
#include <sstream>

using namespace std;

// ---------------------
// long long constructor
// ---------------------

TEST(IntegerFixture, longlong_constructor_zero_1) {
    Integer<char> x = 0;
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, longlong_constructor_zero_2) {
    Integer<char> x = -0;
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, longlong_constructor_negative_small_num) {
    Integer<char> x = -1;
    ASSERT_EQ(x, 0 - 1);
}

TEST(IntegerFixture, longlong_constructor_positive_small_num) {
    Integer<char> x = 1;
    ASSERT_EQ(x, 1);
}

TEST(IntegerFixture, longlong_constructor_positive_large_num) {
    long long value = 12345678912345678;
    Integer<char> x = value;
    ASSERT_EQ(x, value);
}

TEST(IntegerFixture, longlong_constructor_negative_large_num) {
    long long value = -12345678912345678;
    Integer<char> x = value;
    ASSERT_EQ(x, value);
}

// ------------------
// string constructor
// ------------------

TEST(IntegerFixture, string_constructor_zero_1) {
    Integer<char> x ("0");
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, string_constructor_zero_2) {
    Integer<char> x ("-0");
    ASSERT_EQ(x, 0);
}

TEST(IntegerFixture, string_constructor_negative_small_num) {
    Integer<char> x ("-1");
    ASSERT_EQ(x, -1);
}

TEST(IntegerFixture, string_constructor_positive_small_num) {
    Integer<char> x ("1");
    ASSERT_EQ(x, 1);
}

TEST(IntegerFixture, string_constructor_positive_large_num) {
    Integer<char> x ("1234567891234");

    ASSERT_EQ(x, 1234567891234);
}

TEST(IntegerFixture, string_constructor_negative_large_num) {
    Integer<char> x ("-1234567891234");
    ASSERT_EQ(x, -1234567891234);
}

// -----------
// plus_digits
// -----------

TEST(IntegerFixture, plus_1) {
    Integer<char> x = 1;
    Integer<char> y = 1;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 2);
}

TEST(IntegerFixture, plus_2) {
    Integer<char> x = 1024;
    Integer<char> y = 2048;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 3072);
}

TEST(IntegerFixture, plus_3) {
    Integer<char> x = 100000000000;
    Integer<char> y = 99999999999;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 199999999999);
}

TEST(IntegerFixture, plus_4) {
    Integer<char> x = 100000000000;
    Integer<char> y = -99999999999;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 1);
}

TEST(IntegerFixture, plus_5) {
    Integer<char> x = -100000000000;
    Integer<char> y = -99999999999;
    Integer<char> z = x + y;
    ASSERT_EQ(z, -199999999999);
}

TEST(IntegerFixture, plus_6) {
    Integer<char> x = -99999999999;
    Integer<char> y = -100000000000;
    Integer<char> z = x + y;
    ASSERT_EQ(z, -199999999999);
}

TEST(IntegerFixture, plus_7) {
    Integer<char> x = 0;
    Integer<char> y = 0;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, plus_8) {
    Integer<char> x = 1000000000;
    Integer<char> y = -1000000000;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, plus_9) {
    Integer<char> x = 124213513;
    Integer<char> y = 32423414;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 156636927);
}

TEST(IntegerFixture, plus_10) {
    Integer<char> x = 98237;
    Integer<char> y = 781246;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 879483);
}

// ------------
// minus_digits
// ------------

TEST(IntegerFixture, minus_1) {
    Integer<char> x = 0;
    Integer<char> y = 0;
    Integer<char> z = x - y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, minus_2) {
    Integer<char> x = 1;
    Integer<char> y = -1;
    Integer<char> z = x - y;
    ASSERT_EQ(z, 2);
}

TEST(IntegerFixture, minus_3) {
    Integer<char> x = -10086;
    Integer<char> y = -1;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -10085);
}

TEST(IntegerFixture, minus_4) {
    Integer<char> x = 112;
    Integer<char> y = 9901;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -9789);
}

TEST(IntegerFixture, minus_5) {
    Integer<char> x = -112;
    Integer<char> y = -112;
    Integer<char> z = x - y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, minus_6) {
    Integer<char> x = 112;
    Integer<char> y = 112;
    Integer<char> z = x - y;
    ASSERT_EQ(z, 0);
}

// -----------------
// multiplies_digits
// -----------------

TEST(IntegerFixture, multiply_1) {
    Integer<char> x = 0;
    Integer<char> y = -1;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, multiply_2) {
    Integer<char> x = -1;
    Integer<char> y = 0;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, multiply_3) {
    Integer<char> x = 1124132512;
    Integer<char> y = 0;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, multiply_4) {
    Integer<char> x = 0;
    Integer<char> y = 141235125;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, multiply_5) {
    Integer<char> x = 0;
    Integer<char> y = -141235125;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, multiply_6) {
    Integer<char> x = 1;
    Integer<char> y = 141235125;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 141235125);
}

TEST(IntegerFixture, multiply_7) {
    Integer<char> x = 141235125;
    Integer<char> y = -1;
    Integer<char> z = x * y;
    ASSERT_EQ(z, -141235125);
}

TEST(IntegerFixture, multiply_8) {
    Integer<char> x = 962814;
    Integer<char> y = 98153;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 94503082542);
}

TEST(IntegerFixture, multiply_9) {
    Integer<char> x = -893523;
    Integer<char> y = 123;
    Integer<char> z = x * y;
    ASSERT_EQ(z, -109903329);
}

TEST(IntegerFixture, multiply_10) {
    Integer<char> x = -8527;
    Integer<char> y = -4444;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 37893988);
}

// ---
// pow
// ---

TEST(IntegerFixture, pow_1) {
    Integer<char> x = 0;
    Integer<char> z = x.pow(1);
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, pow_2) {
    Integer<char> x = 1;
    Integer<char> z = x.pow(100);
    ASSERT_EQ(z, 1);
}

TEST(IntegerFixture, pow_3) {
    Integer<char> x = 2;
    Integer<char> z = x.pow(6);
    ASSERT_EQ(z, 64);
}

TEST(IntegerFixture, pow_4) {
    Integer<char> x = -142;
    Integer<char> z = x.pow(5);
    ASSERT_EQ(z, -57735339232);
}

TEST(IntegerFixture, pow_5) {
    Integer<char> x = 100;
    Integer<char> z = x.pow(0);
    ASSERT_EQ(z, 1);
}

TEST(IntegerFixture, pow_6) {
    Integer<char> x = 120;
    Integer<char> z = x.pow(5);
    ASSERT_EQ(z, 24883200000);
}

// ---
// abs
// ---

TEST(IntegerFixture, abs_1) {
    Integer<char> x = 0;
    Integer<char> z = x.abs();
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, abs_2) {
    Integer<char> x = 1;
    Integer<char> z = x.abs();
    ASSERT_EQ(z, 1);
}

TEST(IntegerFixture, abs_3) {
    Integer<char> x = -1;
    Integer<char> z = x.abs();
    ASSERT_EQ(z, 1);
}

// ------------------
// shift_right_digits
// ------------------

TEST(IntegerFixture, right_shift_1) {
    Integer<char> x = 1024;
    Integer<char> z = x >> 1;
    ASSERT_EQ(z, 102);
}

TEST(IntegerFixture, right_shift_2) {
    Integer<char> x = -1024;
    Integer<char> z = x >> 1;
    ASSERT_EQ(z, -102);
}

TEST(IntegerFixture, right_shift_3) {
    Integer<char> x = 0;
    Integer<char> z = x >> 1;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, right_shift_4) {
    Integer<char> x ("0");
    Integer<char> z = x >> 1;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, right_shift_5) {
    Integer<char> x ("-0");
    Integer<char> z = x >> 100;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, right_shift_6) {
    Integer<char> x = 234;
    Integer<char> z = x >> 3;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, right_shift_7) {
    Integer<char> x = 234;
    Integer<char> z = x >> 0;
    ASSERT_EQ(z, 234);
}

// -----------------
// shift_left_digits
// -----------------

TEST(IntegerFixture, left_shift_1) {
    Integer<char> x = 0;
    Integer<char> z = x << 1;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, left_shift_2) {
    Integer<char> x = -0;
    Integer<char> z = x << 10;
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, left_shift_3) {
    Integer<char> x ("-28");
    Integer<char> z = x << 0;
    ASSERT_EQ(z, -28);
}

// ------------------
// pre-incrementation
// ------------------

TEST(IntegerFixture, pre_increment_1) {
    Integer<char> x = -1;
    Integer<char> z = ++x;
    ASSERT_EQ(x, 0);
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, pre_increment_2) {
    Integer<char> x = 0;
    Integer<char> z = ++x;
    ASSERT_EQ(x, 1);
    ASSERT_EQ(z, 1);
}

TEST(IntegerFixture, pre_increment_3) {
    Integer<char> x = 22;
    Integer<char> z = ++++x;
    ASSERT_EQ(x, 24);
    ASSERT_EQ(z, 24);
}

// -------------------
// post-incrementation
// -------------------

TEST(IntegerFixture, post_increment_1) {
    Integer<char> x = 12;
    Integer<char> v = x++;
    ASSERT_EQ(x, 13);
    ASSERT_EQ(v, 12);
    ASSERT_EQ(v < x, true);
}

TEST(IntegerFixture, post_increment_2) {
    Integer<char> x = 0;
    Integer<char> z = x++;
    ASSERT_EQ(x, 1);
    ASSERT_EQ(z, 0);
    ASSERT_EQ(x > z, true);
}

TEST(IntegerFixture, post_increment_3) {
    Integer<char> x = 22;
    Integer<char> z = (++x)++;
    ASSERT_EQ(x, 24);
    ASSERT_EQ(z, 23);
    ASSERT_EQ(x != z, true);
}

// ------------------
// pre-decrementation
// ------------------

TEST(IntegerFixture, pre_decrement_1) {
    Integer<char> x = 1;
    Integer<char> z = --x;
    ASSERT_EQ(x, 0);
    ASSERT_EQ(z, 0);
}

TEST(IntegerFixture, pre_decrement_2) {
    Integer<char> x = 0;
    Integer<char> z = --x;
    ASSERT_EQ(x, -1);
    ASSERT_EQ(z, -1);
}

TEST(IntegerFixture, pre_decrement_3) {
    Integer<char> x = 22;
    Integer<char> z = ----x;
    ASSERT_EQ(x, 20);
    ASSERT_EQ(z, 20);
}

// -------------------
// post-decrementation
// -------------------

TEST(IntegerFixture, post_decrement_1) {
    Integer<char> x = 12;
    Integer<char> v = x--;
    ASSERT_EQ(x, 11);
    ASSERT_EQ(v, 12);
    ASSERT_EQ(v < x, false);
}

TEST(IntegerFixture, post_decrement_2) {
    Integer<char> x = 0;
    Integer<char> z = x--;
    ASSERT_EQ(x, -1);
    ASSERT_EQ(z, 0);
    ASSERT_EQ(x < z, true);
}

TEST(IntegerFixture, post_decrement_3) {
    Integer<char> x = 22;
    Integer<char> z = (--x)--;
    ASSERT_EQ(x, 20);
    ASSERT_EQ(z, 21);
    ASSERT_EQ(x < z, true);
}