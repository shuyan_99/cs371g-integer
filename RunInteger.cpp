// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream
#include <sstream>
#include <iterator>
#include <tuple>

#include "Integer.hpp"

using namespace std;

// ------------
// integer_read
// ------------

tuple<string, string, string> integer_read(string &line) {

    auto b = line.begin();
    auto e = line.end();
    string l;
    string r;
    string op;

    // the first number (lhs)
    if (*b == '-') {
        l += *b;
        ++b;
    }
    while ((*b != '+') && (*b != '-') && (*b != '*') && (*b != ' ')) {
        l += *b;
        ++b;
    }

    // skip space
    ++b;

    op += *b;
    ++b;
    if (*b == '*') {
        op += *b;
        // skip "*" and space
        ++++b;
    } else {
        // skip space
        ++b;

        // get the second number (rhs) if not "**"
        while (b != e) {
            r += *b;
            ++b;
        }
    }

    // get the second number (rhs) for the case of "**"
    while (b != e) {
        r += *b;
        ++b;
    }
    return make_tuple(l, op, r);
}

// ------------
// integer_eval
// ------------

Integer<char> integer_eval(tuple<string, string, string> input) {

    // get the arguments
    string l;
    string op;
    string r;
    tie(l, op, r) = input;
    Integer<char> lhs(l);
    Integer<char> rhs(r);

    // do calculations
    Integer<char> result = 0;
    if (op == "+") {
        result = lhs + rhs;
    } else if (op == "-") {
        result = lhs - rhs;
    } else if (op == "*") {
        result = lhs * rhs;
    } else {
        result = lhs.pow(stoi(r));
    }

    return result;
}

// -------------
// integer_print
// -------------

void integer_print(ostream& sout, const Integer<char>& result) {
    sout << result << endl;
}

// -------------
// integer_solve
// -------------

void integer_solve (string line, ostream& sout) {
    integer_print(sout, integer_eval(integer_read(line)));
}

// ----
// main
// ----

int main () {
    string line;
    while (getline(cin, line)) {
        integer_solve(line, cout);
    }
    return 0;
}
