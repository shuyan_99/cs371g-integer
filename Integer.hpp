// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // vector
#include <algorithm> // find

using namespace std;

// Have to declare minus_digits here before plus_digits (or the compiler would complain).
// since we call minus_digits in plus_digits and plus_digits is written before minus_digits in this file.
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x);

// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {

    // n == 0 case handled before calling this function
    assert(n > 0);

    // sign digit
    *x = *b;
    ++x;
    ++b;

    bool all_zeros = true;
    for (auto i = b; i != e && all_zeros; ++i) {
        if (*i != 0) {
            all_zeros = false;
        }
    }
    assert(!all_zeros);

    // append n 0s after the sign digit since actual digits are stored in reverse order
    while (n != 0) {
        *x = 0;
        ++x;
        --n;
    }

    // copy over the actual digits
    while (b != e) {
        *x = *b;
        ++b;
        ++x;
    }

    return x;
}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {

    // n == 0 case handled before calling this function
    assert(n > 0);

    // sign digit
    *x = *b;
    ++b;
    ++x;

    bool all_zeros = true;
    for (auto i = b; i != e && all_zeros; ++i) {
        if (*i != 0) {
            all_zeros = false;
        }
    }
    assert(!all_zeros);

    // skip n digits
    b += n;

    // copy over the rest of the actual digits
    while (b != e) {
        *x = *b;
        ++b;
        ++x;
    }

    // distance(b, e) == n
    if (b == e) {
        *x = 0;
        ++x;
    }

    return x;
}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {

    // copy the numbers into v1, v2 such that
    // we know we have random access iterators and can alter iterator contents
    int length1 = distance(b1, e1);
    int length2 = distance(b2, e2);
    vector<char> v1(length1);
    vector<char> v2(length2);
    copy(b1, e1, v1.begin());
    copy(b2, e2, v2.begin());

    // if the two numbers have different signs,
    // change the sign in v1 and v2, and let minus_digits handle the calculation
    if (*b1 == 1 && *b2 == 0) {
        *v1.begin() = 0;
        return minus_digits(v2.begin(), v2.end(), v1.begin(), v1.end(), x);
    } else if (*b1 == 0 && *b2 == 1) {
        *v2.begin() = 0;
        return minus_digits(v1.begin(), v1.end(), v2.begin(), v2.end(), x);
    }

    // find the shorter one, assume v2 is longer, switch if v1 is longer
    auto long_b = v2.begin();
    auto long_e = v2.end();
    auto short_b = v1.begin();
    auto short_e = v1.end();
    if (length1 > length2) {
        short_b = v2.begin();
        short_e = v2.end();
        long_b = v1.begin();
        long_e = v1.end();
    }

    // set sign digit and skip it
    // (v1 and v2 must have the same sign now)
    *x = *short_b;
    ++short_b;
    ++long_b;
    ++x;

    // do the calculation with carrying
    int carry = 0;
    while (short_b != short_e) {
        int sum = *short_b + *long_b + carry;
        carry = 0;
        if (sum >= 10) {
            carry = 1;
            sum %= 10;
        }
        *x = sum;
        ++x;
        ++short_b;
        ++long_b;
    }
    // the digits left in long_v
    while (long_b != long_e) {
        int sum = *long_b + carry;
        carry = 0;
        if (sum >= 10) {
            carry = 1;
            sum %= 10;
        }
        *x = sum;
        ++x;
        ++long_b;
    }
    // the last carry
    if (carry == 1) {
        *x = 1;
    }
    return x;
}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {

    int length1 = distance(b1, e1);
    int length2 = distance(b2, e2);
    vector<char> v1(length1);
    vector<char> v2(length2);
    std::copy(b1, e1, v1.begin());
    std::copy(b2, e2, v2.begin());

    // different signs, pass sequence to plus
    // a - -b = a + b, guaranteed positive
    if (*v1.begin() == 0 && *v2.begin() == 1) {
        *v2.begin() = 0;
        return plus_digits(v1.begin(), v1.end(), v2.begin(), v2.end(), x);
        // -a - b = -a + -b, guaranteed negative
    } else if(*v1.begin() == 1 && *v2.begin() == 0) {
        *v2.begin() = 1;
        return plus_digits(v1.begin(), v1.end(), v2.begin(), v2.end(), x);
    }

    // same sign, do subtraction, always use larger abs - smaller abs
    // If second sequence is larger, result is negative
    // assume v2 is larger first, then (v1 - v2) must be negative => *x = 1
    *x = 1;
    auto long_b = v2.begin();
    auto long_e = v2.end();
    auto short_b = v1.begin();
    auto short_e = v1.end();

    // find the longer digit sequence
    // if v1 is longer, switch the two and do *x = 0 since (v1 - v2) must be positive
    if (length1 > length2) {
        short_b = v2.begin();
        short_e = v2.end();
        long_b = v1.begin();
        long_e = v1.end();
        *x = 0;
    }

    // if lengths are the same, find the larger sequence
    // we assume v2 is longer, then we compare each pair of digits
    // starting from the end of the two sequences (the most significant ones)
    if (length1 == length2) {
        auto temp_e1 = v1.end();
        auto temp_e2 = v2.end();
        --temp_e1;
        --temp_e2;
        while (v1.begin() != temp_e1) {
            if (*temp_e1 != *temp_e2) {
                // v1 is larger (or, "longer", in our representation)
                if (*temp_e1 > *temp_e2) {
                    short_b = v2.begin();
                    short_e = v2.end();
                    long_b = v1.begin();
                    long_e = v1.end();
                    *x = 0;
                }
                break;
            }
            --temp_e1;
            --temp_e2;
        }
        // two sequence are the same, set all digits to zero
        if (v1.begin() == temp_e1) {
            fill(x, x + length1, 0);
            return x += length1;
        }
    }

    // if two sequence are both negative, change the sign of output
    // (x - y = +output) => (-x - -y = y - x = -output)
    if (*v1.begin() == 1 && *v2.begin() == 1) {
        *x = !*x;
    }

    // skip sign digit
    ++x;
    ++short_b;
    ++long_b;

    // do the calculation with carrying
    int carry = 0;
    while (short_b != short_e) {
        int diff = *long_b - carry - *short_b;
        if (diff >= 0) {
            *x = diff;
            carry = 0;
        } else {
            *x = 10 - *short_b + *long_b - carry;
            carry = 1;
        }
        ++short_b;
        ++long_b;
        ++x;
    }
    // handle the digits left in long_v
    while (long_b != long_e) {
        if (carry == 1) {
            if (*long_b == 0) {
                *x = 9;
            } else {
                *x = *long_b -carry;
                carry = 0;
            }
        } else {
            *x = *long_b;
        }
        ++long_b;
        ++x;
    }
    return x;
}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * https://en.wikipedia.org/wiki/Sch%C3%B6nhage%E2%80%93Strassen_algorithm
 * ([b1, e1) * [b2, e2)) => x
 */

template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    // signs
    if (*b1 != *b2) {
        *x = 1;
    } else {
        *x = 0;
    }
    ++x;
    ++b1;
    ++b2;

    // length1 and length2 does not include signs
    int length1 = distance(b1, e1);
    int length2 = distance(b2, e2);

    // find the shorter one, assume v2 is longer, switch if v1 is longer
    vector<char> long_v(length2);
    std::copy(b2, e2, long_v.begin());
    vector<char> short_v(length1);
    std::copy(b1, e1, short_v.begin());
    if (length1 > length2) {
        long_v.clear();
        long_v.resize(length1);
        std::copy(b1, e1, long_v.begin());
        short_v.clear();
        short_v.resize(length2);
        std::copy(b2, e2, short_v.begin());
    }

    bool is_zero1 = true;
    bool is_zero2 = true;
    for (int i : long_v) {
        if (i != 0) {
            is_zero1 = false;
        }
    }
    for (int i : short_v) {
        if (i != 0) {
            is_zero2 = false;
        }
    }
    // if one operand is zero
    if (is_zero1 || is_zero2) {
        // get rid of the -0 case, turn the sign digit of result into + (which means first digit is 0)
        *(x - 1) = 0;
        // result is 0
        *x = 0;
        ++x;
        // clear all non-zero values at and after x for the accuracy of size calculation
        auto temp = x;
        while (*temp != 0) {
            *temp = 0;
            ++temp;
        }
        return x;
    }

    // a naive implementation of Schönhage–Strassen algorithm
    vector<vector<int>> vectors(short_v.size());
    int num_vectors = short_v.size();
    for (int i = 0; i < num_vectors; ++i) {
        for (int j = 0; j < i; ++j) {
            vectors[i].push_back(0);
        }
    }
    for (int i = 0; i < num_vectors; ++i) {
        for (int j : long_v) {
            vectors[i].push_back(j * short_v[i]);
        }
    }
    for (int i = 0; i < num_vectors; ++i) {
        for (int j = 0; j < num_vectors - i - 1; ++j) {
            vectors[i].push_back(0);
        }
    }
    vector<int> sum_vectors(vectors[0].size());
    int sum_vectors_size = sum_vectors.size();
    for (int i = 0; i < sum_vectors_size; ++i) {
        int temp_sum_at_index_i = 0;
        for (int j = 0; j < num_vectors; ++j) {
            temp_sum_at_index_i += vectors[j][i];
        }
        sum_vectors[i] = temp_sum_at_index_i;
    }
    int carry = 0;
    for (auto it = sum_vectors.begin(); it != sum_vectors.end(); ++it) {
        int digit_to_write = (*it + carry) % 10;
        *x = digit_to_write;
        ++x;
        carry = (*it + carry) / 10;
    }
    if (carry) {
        *x = carry;
        ++x;
    }

    return x;
}

// -------
// Integer
// -------

template <typename T, typename C = std::vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if the two Integers have the same value, false otherwise
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {

        auto blhs = lhs._x.begin();
        auto brhs = rhs._x.begin();

        // +0 == +0; +0 = -0; -0 = -0
        if (*(blhs + 1) == 0 && *(brhs + 1) == 0 && lhs.size == 1 && rhs.size == 1) {
            return true;
        }

        // if different signs or size not equal, the two numbers are not equal
        if (*blhs != *brhs || lhs.size != rhs.size) {
            return false;
        }

        // skip sign digit
        ++blhs;
        ++brhs;

        // traverse through the numbers to check
        // if any two corresponding digits are different
        for (size_t i = 0; i < lhs.size; ++i) {
            if (*blhs != *brhs) {
                return false;
            }
            ++blhs;
            ++brhs;
        }
        return true;
    }

    // -----------
    // operator !=
    // -----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if the two Integers have different values, false otherwise
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if (the value of lhs) < (the value of rhs), false otherwise
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        // different sign
        if (lhs.negative != rhs.negative) {
            return lhs.negative;
        }

        // same sign
        if (lhs.size != rhs.size) {
            bool l_shorter_than_r = (rhs.size > lhs.size);
            if (lhs.negative) {
                return (!l_shorter_than_r);
            } else {
                return (l_shorter_than_r);
            }
        }
        // same sign, same size
        auto blhs = lhs._x.begin();
        auto brhs = rhs._x.begin();

        blhs += lhs.size;
        brhs += rhs.size;

        for (int i = 0; i < (int)lhs.size; ++i) {
            if (*blhs < *brhs) {
                return true;
            }
            --blhs;
            --brhs;
        }
        return false;
    }

    // -----------
    // operator <=
    // -----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if (the value of lhs) <= (the value of rhs), false otherwise
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);
    }

    // ----------
    // operator >
    // ----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if (the value of lhs) > (the value of rhs), false otherwise
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);
    }

    // -----------
    // operator >=
    // -----------

    /**
     * @param lhs a reference to the left hand side Integer in the comparison
     * @param rhs a reference to the right hand side Integer in the comparison
     * @return true if (the value of lhs) >= (the value of rhs), false otherwise
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);
    }

    // ----------
    // operator +
    // ----------

    /**
     * @param lhs an Integer on the left hand side of the "+" op
     * @param rhs a reference to the right hand side Integer
     * @return the result of (lhs + rhs)
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;
    }

    // ----------
    // operator -
    // ----------

    /**
     * @param lhs an Integer on the left hand side of the "-" op
     * @param rhs a reference to the right hand side Integer
     * @return the result of (lhs - rhs)
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;
    }

    // ----------
    // operator *
    // ----------

    /**
     * @param lhs an Integer on the left hand side of the "*" op
     * @param rhs a reference to the right hand side Integer
     * @return the result of (lhs * rhs)
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs an Integer on the left hand side of the "<<" op
     * @param rhs an int specifying how many digits to shift
     * @return the result of (lhs << rhs)
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        if (rhs < 0) {
            throw invalid_argument("rhs < 0");
        }
        return lhs <<= rhs;
    }

    // -----------
    // operator >>
    // -----------

    /**
     * @param lhs an Integer on the left hand side of the "<<" op
     * @param rhs an int specifying how many digits to shift
     * @return the result of (lhs >> rhs)
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        if (rhs < 0) {
            throw invalid_argument("rhs < 0");
        }
        return lhs >>= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * @param lhs a reference to std::ostream
     * @param rhs a reference to a const Integer object
     * @return the lhs ostream and print out the rhs Integer
     */
    friend std::ostream& operator << (ostream& lhs, const Integer& rhs) {
        if (*rhs._x.begin() == 1) {
            lhs << "-";
        }
        auto end = rhs._x.begin() + rhs.size;
        while (end != rhs._x.begin()) {
            lhs << (int)*end;
            --end;
        }
        return lhs;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * @param an Integer x
     * @return another Integer, whose container holds the absolute value of x
     */
    friend Integer abs (Integer x) {
        return x.abs();
    }

    // ---
    // pow
    // ---

    /**
     * power
     * @param an Integer x
     * @param an int e
     * @return another Integer, whose container holds the result of Integer x to the eth power
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        if ((x == 0) && (e == 0)) {
            throw invalid_argument("(x == 0) && (e == 0)");
        }
        if (e < 0) {
            throw invalid_argument("e < 0");
        }
        return x.pow(e);
    }

public:

    // ----
    // data
    // ----

    C _x;             // the backing container
    size_t size = 0;  // without sign
    int negative;     // negative = 1 if Integer is negative, 0 otherwise

private:

    // ----------------
    // recalculate_data
    // ----------------

    void recalculate_data() {

        // update negativity
        if (*_x.begin() == 1) {
            negative = 1;
        } else {
            negative = 0;
        }

        // recalculate size (to not count the zeros at the end of the container)
        auto e = _x.end();

        // account for sign digit, sign digit excluded from size calculation
        --e;

        while (e != _x.begin()) {

            if (*e != 0) {
                size = distance(_x.begin(), e);
                // clear all digits after size
                auto start = _x.begin() + size + 1;
                while (start != _x.end()) {
                    *start = 0;
                    ++start;
                }
                return;
            }
            --e;
        }

        // minimum size
        size = 1;

        // clear all digits after size
        auto start = _x.begin() + size + 1;
        while (start != _x.end()) {
            *start = 0;
            ++start;
        }
    }

    // -----
    // valid
    // -----

    bool valid () const {
        auto b = _x.begin();
        auto e = _x.end();
        if (b == e) {
            return false;
        }

        if (*b) {
            assert(negative == 1);
        } else {
            assert(negative == 0);
        }

        // skip sign digit
        ++b;
        // size is at least 1 for all integers
        assert(size >= 1);
        if (b == e) {
            return false;
        }

        size_t count = 0;
        while (b != e) {
            if ((int)*b >= 10) {
                return false;
            }
            ++count;
            ++b;
        }
        // capacity >= size
        assert(count >= size);
        return true;
    }

public:

    // ------------
    // constructors
    // ------------

    /**
     * implicit contructor for an Integer object
     * @param a long long "value"
     * initialize container _x, size, and negative
     */
    Integer (long long value) {

        // count the number of digits
        // to decide how big the container should be ((size + 1)*2)
        long long temp_value = value;
        if (value == 0) {
            size = 1;
        } else {
            while (temp_value != 0) {
                temp_value /= 10;
                ++size;
            }
        }
        _x.resize((size + 1) * 2);

        auto b = _x.begin();
        // sign digit
        *b = 0;
        negative = 0;
        if (value < 0) {
            *b = 1;
            value = -value;
            negative = 1;
        }
        ++b;

        // handle the 0 case
        if (value == 0) {
            *b = 0;
            ++b;
        }
        // handle the non-zero case
        while (value != 0) {
            *b = value % 10;
            value /= 10;
            ++b;
        }

        assert(valid());
    }

    /**
     * explicit contructor for an Integer object
     * @param a reference to a const string "value" that represents the Integer to be built
     * initialize container _x, size, and negative
     * @throws invalid_argument if value is not a valid representation of an Integer
     */
    explicit Integer (const std::string& value) {

        // resize the container
        _x.resize((value.size() + 1) * 2);

        auto b = _x.begin();

        // handle the 0 case separately
        if (value == "0" || value == "-0") {
            size = 1;
            negative = 0;
            *b = 0;
            *(b + 1) = 0;
            ++++b;
            return;
        }

        b = _x.begin();
        // sign digit
        int start = 0;
        *b = 0;
        negative = 0;
        if (value[0] == '-') {
            *b = 1;
            start = 1;
            negative = 1;
        }
        ++b;

        // use start to handle either with or without "-"
        size = value.size() - start;
        vector<char> valid_digits { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ' };
        for (int i = value.size() - 1; i >= start; --i) {
            if (find(valid_digits.begin(), valid_digits.end(), value[i]) == valid_digits.end()) {
                throw invalid_argument("value is not a valid representation of an Integer");
            }
            *b = value[i] - '0';
            ++b;
        }

        assert(valid());
    }

    Integer             (const Integer&) = default;
    ~Integer            ()               = default;
    Integer& operator = (const Integer&) = default;

    // ----------
    // operator -
    // ----------

    /**
     * @return another Integer that has the opposite sign but the same value with this Integer
     */
    Integer operator - () const {
        Integer x = *this;
        x.negative = !(x.negative);
        *(x._x.begin()) = x.negative;
        assert(valid());
        return x;
    }

    // -----------
    // operator ++
    // -----------

    /**
     * pre-incrementation
     * increment this Integer by 1
     * @return a reference to this Integer after its incrementation
     */
    Integer& operator ++ () {
        *this += 1;
        assert(valid());
        return *this;
    }

    /**
     * post-incrementation
     * increment this Integer by 1
     * @return another Integer that holds the un-incremented value of this Integer
     */
    Integer operator ++ (int) {
        Integer x = *this;
        *this += 1;
        assert(valid());
        return x;
    }

    // -----------
    // operator --
    // -----------

    /**
     * pre-decrementation
     * decrement this Integer by 1
     * @return a reference to this Integer after its decrementation
     */
    Integer& operator -- () {
        *this -= 1;
        assert(valid());
        return *this;
    }

    /**
     * post-decrementation
     * decrement this Integer by 1
     * @return another Integer that holds the un-decremented value of this Integer
     */
    Integer operator -- (int) {
        Integer x = *this;
        *this -= 1;
        assert(valid());
        return x;
    }

    // -----------
    // operator +=
    // -----------

    /**
     * @param a reference to a const Integer "rhs"
     * @return a reference to this Integer, which holds the result of (this + rhs)
     */
    Integer& operator += (const Integer& rhs) {

        // resize container
        if (_x.size() < (max(size, rhs.size) + 1)) {
            _x.resize((max(size, rhs.size) + 1) * 2);
        }
        // get the end of both containers and perform addition
        auto e1 = _x.begin() + size + 1;
        auto e2 = rhs._x.begin() + rhs.size + 1;
        plus_digits(_x.begin(), e1, rhs._x.begin(), e2, _x.begin());

        recalculate_data();
        assert(valid());
        return *this;
    }

    // -----------
    // operator -=
    // -----------

    /**
     * @param a reference to a const Integer "rhs"
     * @return a reference to this Integer, which holds the result of (this - rhs)
     */
    Integer& operator -= (const Integer& rhs) {

        // resize container
        if (_x.size() < (max(size, rhs.size) + 1)) {
            _x.resize((max(size, rhs.size) + 1) * 2);
        }

        // get the end of both containers and perform subtraction
        auto e1 = _x.begin() + size + 1;
        auto e2 = rhs._x.begin() + rhs.size + 1;
        minus_digits(_x.begin(), e1, rhs._x.begin(), e2, _x.begin());

        recalculate_data();
        assert(valid());
        return *this;
    }

    // -----------
    // operator *=
    // -----------

    /**
     * @param a reference to a const Integer "rhs"
     * @return a reference to this Integer, which holds the result of (this * rhs)
     */
    Integer& operator *= (const Integer& rhs) {

        // resize container
        _x.resize(2 * (size + rhs.size));

        // get the end of both containers and perform subtraction
        auto e1 = _x.begin() + size + 1;
        auto e2 = rhs._x.begin() + rhs.size + 1;
        multiplies_digits(_x.begin(), e1, rhs._x.begin(), e2, _x.begin());

        recalculate_data();
        assert(valid());
        return *this;
    }

    // ------------
    // operator <<=
    // ------------

    /**
     * @param a reference to a const Integer "rhs"
     * @return a reference to this Integer, which holds the result of (this << rhs)
     */
    Integer& operator <<= (int n) {

        assert((0 <= n));

        if (n == 0) {
            return *this;
        }

        // resize container
        if (_x.size() < (size + n)) {
            _x.resize(2*(size + n));
        }

        // handle 0 case
        if (*(_x.begin() + 1) == 0 && size == 1) {
            return *this;
        }

        vector<T> temp = (*this)._x;

        // handle non zero case
        // note that we cannot do the comment below due to the inner machinery of left-shifting
        // "shift_left_digits(_x.begin(), _x.end(), n, _x.begin())"
        shift_left_digits(temp.begin(), temp.end(), n, _x.begin());
        size += n;

        assert(size > 0);
        assert(valid());
        return *this;
    }

    // ------------
    // operator >>=
    // ------------

    /**
     * @param a reference to a const Integer "rhs"
     * @return a reference to this Integer, which holds the result of (this >> rhs)
     */
    Integer& operator >>= (int n) {

        assert((0 <= n) && (size - n >= 0));

        if (n == 0) {
            return *this;
        }

        // handle 0 case
        if (*(_x.begin() + 1) == 0 && size == 1) {
            return *this;
        }
        // handle non zero case
        shift_right_digits(_x.begin(), _x.end(), n, _x.begin());
        size -= n;

        // i.e. 234 >> 3 -> 0.
        // The size (or length) of zero is 1, since there is 1 non-sign digit.
        if (size == 0) {
            size = 1;
        }

        assert(size > 0);
        assert(valid());
        return *this;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * @return a reference to this Integer, which is the absolute value of its current version
     */
    Integer& abs () {
        *(_x.begin()) = 0;
        negative = 0;
        assert(valid());
        return *this;
    }

    // ---
    // pow
    // ---

    /**
     * power
     * @param an int e
     * @return a reference to this Integer, which holds the result of current this Integer to the eth power
     * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
     */
    Integer& pow (int e) {

        if ((this == 0) && (e == 0)) {
            throw invalid_argument("(this == 0) && (e == 0)");
        }

        if (e < 0) {
            throw invalid_argument("e < 0");
        }

        Integer one = 1;

        // x ^ 0 = 1 for all x
        if (e == 0) {
            *this = one;
            return *this;
        }

        // x ^ 1 = x; 1 ^ x = 1 for all x
        if ((e == 1) || (*this == one)) {
            return *this;
        }

        // resize container
        if (_x.size() < (size * e)) {
            _x.resize(2 * (size * e));
        }

        // pow calculation
        Integer temp = *this;
        --e;
        while (e > 0) {
            if (e & 1) {
                *this *= temp;
            }
            e = e >> 1;
            temp *= temp;
        }

        recalculate_data();
        assert(valid());
        return *this;
    }
};

#endif // Integer_h